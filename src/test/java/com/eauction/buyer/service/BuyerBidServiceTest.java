package com.eauction.buyer.service;

import com.eauction.buyer.dto.BuyerBidDetailDTO;
import com.eauction.buyer.entity.BuyerBidDetail;
import com.eauction.buyer.mapper.BuyerBidMapper;
import com.eauction.buyer.repository.BidDetailsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class BuyerBidServiceTest {

    @Mock
    private BidDetailsRepository bidDetailsRepository;
    @Mock
    private BuyerBidMapper buyerBidMapper;


    @InjectMocks
    private BuyerBidService buyerBidService;

    @Test
    void testPlaceBid() {
        BuyerBidDetailDTO buyerBidDetailDTO = populateBidDetailDTO();
        BuyerBidDetail buyerBidDetail = populateBidDetail();
        Mockito.when(buyerBidMapper.fromProductDetailsDTO(buyerBidDetailDTO)).thenReturn(buyerBidDetail);
        BuyerBidDetail response = buyerBidService.placeBid(buyerBidDetailDTO);
        assertEquals("1111-2222",response.getProductId());
    }

    @Test
    void testPlaceBidNullInput() {
        BuyerBidDetail response = buyerBidService.placeBid(null);
        assertNull(response);
    }

    @Test
    void testUpdateBid() {
        String productId = "1111-2222";
        String emailId = "bravin@gmail.com";
        String bidAmount = "3000";
        BuyerBidDetail buyerBidDetail = populateBidDetail();
        buyerBidDetail.setBidAmount(bidAmount);
        Mockito.when(bidDetailsRepository.updateBid(productId,emailId,bidAmount)).thenReturn(buyerBidDetail);
        BuyerBidDetail response = buyerBidService.updateBid(productId,emailId,bidAmount);
        assertEquals(buyerBidDetail, response);
    }

    @Test
    void testUpdateBidInvalidNullInput() {
        String productId = "1111-2222";
        BuyerBidDetail response = buyerBidService.updateBid(productId,null,null);
        assertNull(response);
    }

    private BuyerBidDetailDTO populateBidDetailDTO() {
        BuyerBidDetailDTO buyerBidDetailDTO = new BuyerBidDetailDTO();
        buyerBidDetailDTO.setProductId("1111-2222");
        buyerBidDetailDTO.setBidAmount("2000");
        buyerBidDetailDTO.setFirstName("Bravin");
        buyerBidDetailDTO.setLastName("Ninja");
        buyerBidDetailDTO.setAddress("Chennai");
        buyerBidDetailDTO.setEmail("bravin@gmail.com");
        buyerBidDetailDTO.setPhone(1234567890);
        buyerBidDetailDTO.setCity("Chennai");
        buyerBidDetailDTO.setPin("123456");
        buyerBidDetailDTO.setState("TN");
        return buyerBidDetailDTO;
    }

    private BuyerBidDetail populateBidDetail() {
        BuyerBidDetail buyerBidDetail = new BuyerBidDetail();
        buyerBidDetail.setProductId("1111-2222");
        buyerBidDetail.setBidAmount("2000");
        buyerBidDetail.setFirstName("Bravin");
        buyerBidDetail.setLastName("Ninja");
        buyerBidDetail.setAddress("Chennai");
        buyerBidDetail.setEmail("bravin@gmail.com");
        buyerBidDetail.setPhone("1234567890");
        buyerBidDetail.setCity("Chennai");
        buyerBidDetail.setPin("123456");
        buyerBidDetail.setState("TN");
        return buyerBidDetail;
    }


}