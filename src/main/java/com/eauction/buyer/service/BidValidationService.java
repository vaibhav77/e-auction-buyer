package com.eauction.buyer.service;

import com.eauction.buyer.entity.ProductDetail;
import com.eauction.buyer.exception.BidEndedException;
import com.eauction.buyer.exception.DuplicateBidException;
import com.eauction.buyer.repository.BidDetailsRepository;
import com.eauction.buyer.repository.ProductDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.utils.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Log4j2
public class BidValidationService {

    private final BidDetailsRepository bidDetailsRepository;
    private final ProductDetailsRepository productDetailsRepository;

    public boolean validateBidEndDate(String productId) {
        if(!StringUtils.isEmpty(productId)) {
            ProductDetail productDetail = productDetailsRepository.fetchProductById(productId);
            log.info("Fetched product information: {}", productDetail);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate bidDate = LocalDate.parse(productDetail.getBidEndDate(),formatter);
            if(bidDate.isBefore(LocalDate.now())) {
                throw new BidEndedException("Bid cannot be placed or updated. Product Bidding is ended");
            }
        }
        return true;
    }

    public boolean validateBidAssociation(String productId, String email) {
        if(Objects.nonNull(bidDetailsRepository.fetchBids(productId ,email))) {
            throw new DuplicateBidException("Duplicate Bid. Already a Bid is placed for this product");
        }
        return true;
    }
}