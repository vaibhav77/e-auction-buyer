package com.eauction.buyer.dto;

import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class BuyerBidDetailDTO {
    private String productId;
    private String bidAmount;
    @NotNull
    @Size(min=5, max=30, message="First Name length should be between 5 to 30 characters")
    private String firstName;
    @NotNull
    @Size(min=3, max=30, message="Last Name length should be between 3 to 30 characters")
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    @NotNull
    @Digits(integer = 10, fraction = 0)
    private long phone;
    @NotNull
    @Email
    private String email;
}